﻿<?php

define('PHPWIND_FORUM_DIR', PHPWIND_MOD_DIR . '/forum');
require_once PHPWIND_FORUM_DIR . '/data/sql_config.php';

/*
 * Define some Discuz database tables.
 * 
 * */
define('PHPWIND_DB_PREFIX', $PW);
define('PHPWIND_MEMBERS_TABLE', PHPWIND_DB_PREFIX . 'members');
define('PHPWIND_POSTS_TABLE', PHPWIND_DB_PREFIX . 'posts');

/*
 * Define private key between Drupal and Discuz.
 * 
 * */
define('PRIVATE_KEY', variable_get('private_key', 'discuz_drupal_module'));

/*
 * Displays the most recent 10 forum subjects.
 * 
 * */
function _phpwind_block_new_forum_topics()
{
	global $base_url;
	
	$output = '<ul>';

	$result = db_query("SELECT `tid`, `subject` FROM `" . PHPWIND_POSTS_TABLE 
	.  "` WHERE invisible = '%d' ORDER BY `dateline` DESC LIMIT 0, 10", 0);
	while ($obj = db_fetch_object($result)) {
		$output .= '<li>'. l($obj->subject, 
		$base_url . '/modules/phpwind/forum/viewthread.php?tid=' . $obj->tid) 
		. '</li>';
	}
	
	$output .= '</ul>';
	
	return $output;
}

/* 
 * Insert user into PHPWind.
 * 
 * Parsed $edit we can get such as Array ( [name] => sirtoozee 
 * [mail] => sirtoozee@localhost.com 
 * [pass] => PWrcfP2V5N [init] => sirtoozee@localhost.com 
 * [roles] => [status] => 1 [uid] => 2 [created] => 1171342964 )  
 * 
 * */
function _phpwind_user_insert($edit)
{
	db_query("INSERT INTO `" . PHPWIND_MEMBERS_TABLE 
	. "` (`uid`, `username`, `password`, `regdate`, `email`) 
	VALUES ('%d', '%s', '%s', '%d', '%s')", $edit['uid'], $edit['name'], 
	md5($edit['pass']), $edit['created'], $edit['mail']);
}

/*
 * Update user with PHPWind.
 * 
 * Parsed $edit we can ge such as Array ( [timezone] => 28800 
 * [mail] => sirtoozee@localhost.com [pass] => 123456 [signature] => ) 
 * 
 * */
function _phpwind_user_update($edit)
{
	global $user;
	
	// Attention that $user->pass has been md5, but $edit['pass'] hasn`t
	db_query("UPDATE `" . PHPWIND_MEMBERS_TABLE 
	. "` SET password = '%s', email = '%s' WHERE uid = '%d'", 
	!empty($edit['pass']) ? md5($edit['pass']) :$user->pass, 
	$edit['mail'], $user->uid);
}

/*
 * Delete user from PHPWind.
 * 
 * */
function _phpwind_user_delete($uid)
{
	db_query("DELETE FROM `" . PHPWIND_MEMBERS_TABLE . "` WHERE uid = '%d'", $uid);
}

/* 
 * User login.
 * It need PHPWind core devloper's help!
 * 
 * Parsed $account we can get such as $account->name, $account->pass, 
 * $account->mail
 * 
 * */
function _phpwind_user_login($account)
{
	
}

/*
 * User logout.
 * 
 * */
function _phpwind_user_logout($account)
{
	
}

?>